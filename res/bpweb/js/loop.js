var IMG_BG = "/res/bpweb/img/sky.png";
var IMG_PLANES = "/res/bpweb/img/planes_tr.png";
var FPS = 30;
var KEY_SPACE = 32;
var KEY_LEFT = 37;
var KEY_UP = 38;
var KEY_RIGHT = 39;
var KEY_DOWN = 40;
var KEY_P_UPPER = 80;
var KEY_P_LOWER = 112;
var SCREEN_WIDTH = 320;
var SCREEN_HEIGHT = 240;
var PLANE_WIDTH = 32;
var PLANE_HEIGHT = 40;
/** X-coordinate of background */
var backgroundX = 0;
/** Coordinates of hero */
var hero = makeHeroPlane();
var score = 0;
var heroBullets = new Array();
var updatesTillNextEnemy = 60;

var bgImg = new Image();
bgImg.src = IMG_BG;
var heroImg = new Image();
heroImg.src = IMG_PLANES;
var shootSound = new Audio("/res/bpweb/sounds/8bit_gunloop_explosion.wav");

var canvas = null;
var context2D = null;
var keys = new Array();
var enemyPlanes = new Array();
var paused = true;
var started = false;

window.onload = init;

window.addEventListener('click', click, true);

function click(evt) {
	if(!started) {
		started = true;
		paused = false;
	}
}

function keyDown(evt) {
	keys[evt.keyCode] = true;
	evt.preventDefault();
	if(hero.alive) {
		if(evt.keyCode == KEY_SPACE) {
			fireHeroBullet();
			playShootSound();
		}
	}
}

function keyUp(evt) {
	keys[evt.keyCode] = false;
	evt.preventDefault();
	if(hero.alive) {
		if(evt.keyCode == KEY_P_UPPER || evt.keyCode == KEY_P_LOWER) {
			togglePaused();	
		}
	}
}

function init() {
	canvas = document.getElementById('canvas');
	context2D = canvas.getContext('2d');
	setInterval(draw, 1000 / FPS);

	canvas.addEventListener('keydown', keyDown, true);
	canvas.addEventListener('keyup', keyUp, true);
}

function isKeyPressed(code) {
	return (code in keys && keys[code]);
}

function togglePaused() {
	paused = !paused;
}

function draw() {
	update();
	paint();
}

function update() {
	if(!paused && hero.alive) {
		updateHero();
		updateBullets();
		updateEnemyPlanes();
		updateBackground();
		detectCollisions();
	}
}

function paint() {
	if(started) {
		paintScene();
	} else {
		paintStartScreen();
	}
}

function paintScene() {
	context2D.clearRect(0, 0, canvas.width, canvas.height);
	paintBackground();
	paintHero();
	paintHeroBullet();
	paintEnemyPlanes();
	paintMessages();
}

function paintStartScreen() {
	context2D.clearRect(0, 0, canvas.width, canvas.height);
	paintBackground();
	paintMessages();
}

function updateHero() {
	if(isKeyPressed(KEY_LEFT)) {
		moveHeroLeft();
	}
	if(isKeyPressed(KEY_UP)) {
		moveHeroUp();
	}
	if(isKeyPressed(KEY_RIGHT)) {
		moveHeroRight();
	}
	if(isKeyPressed(KEY_DOWN)) {
		moveHeroDown();
	}
}

function moveHeroLeft() {
	if(hero.x > 0) {
		hero.x -= 2;
	}
}

function moveHeroUp() {
	if(hero.y > 0) {
		hero.y -= 2;
	}
}

function moveHeroRight() {
	if(hero.x < SCREEN_WIDTH - PLANE_WIDTH) {
		hero.x += 2;
	}
}

function moveHeroDown() {
	if(hero.y < SCREEN_HEIGHT - PLANE_HEIGHT) {
		hero.y += 2;
	}
}

function updateBackground() {
	backgroundX--;
	if(backgroundX < -SCREEN_WIDTH) {
		backgroundX = 0;
	}
}

function updateBullets() {
	for(var i = 0; i < heroBullets.length; i++) {
		heroBullets[i].x += 3;
		if(heroBullets[i].x > SCREEN_WIDTH) {
			heroBullets[i].alive = false;
		}
	}
}

function updateEnemyPlanes() {
	if(updatesTillNextEnemy-- <= 0) {
		dispatchEnemyPlane();
		updatesTillNextEnemy = 50 + randomInt(20);
	}
	for(var i = 0; i < enemyPlanes.length; i++) {
		enemyPlanes[i].x -= 2;
		if(enemyPlanes[i].x < -PLANE_WIDTH) {
			enemyPlanes[i].alive = false;		
		}
	}
}

function paintBackground() {
	context2D.drawImage(bgImg, backgroundX, 0);
	context2D.drawImage(bgImg, backgroundX + SCREEN_WIDTH, 0);
}

function paintHero() {
	if(hero.alive) {
		context2D.drawImage(heroImg, 0, 0, PLANE_WIDTH, PLANE_HEIGHT, hero.x, hero.y, hero.w, hero.h);
	}
}

function paintHeroBullet() {
	for(var i = 0; i < heroBullets.length; i++) {
		bullet = heroBullets[i];
		if(bullet.alive) {
			context2D.drawImage(heroImg, 96, 0, 8, 8, bullet.x, bullet.y, bullet.w, bullet.h);
		}
	}
}

function paintEnemyPlanes() {
	for(var i = 0; i < enemyPlanes.length; i++) {
		plane = enemyPlanes[i];
		if(plane.alive) {
			var srcX = PLANE_WIDTH * (plane.imgIndex + 1);
			context2D.drawImage(heroImg, srcX, 0, PLANE_WIDTH, PLANE_HEIGHT, plane.x, plane.y, plane.w, plane.h);
		}
	}
}

function paintMessages() {
	paintStart();
	paintPaused();
	paintScore();
	paintGameOver();
}

function paintStart() {
	if(!started) {
		context2D.fillStyle = "black";
		context2D.font = 'bold 30px sans-serif';
		context2D.fillText("CLICK TO START", 25, 130);
	}
}

function paintPaused() {
	if(started && paused) {
		context2D.fillStyle = "black";
		context2D.font = 'bold 30px sans-serif';
		context2D.fillText("PAUSED", 90, 130);
	}
}

function paintScore() {
	context2D.fillStyle = "black";
	context2D.font = 'bold 12px sans-serif';
	context2D.fillText(score, 0, 12);
}

function paintGameOver() {
	if(!hero.alive) {
		context2D.fillStyle = "black";
		context2D.font = 'bold 30px sans-serif';
		context2D.fillText("Game Over", 60, 130);
	}
}

function playShootSound() {
	shootSound.currentTime = 0;
	shootSound.play();
}

function fireHeroBullet() {
	var bullet = makeHeroBullet();
	heroBullets.push(bullet);
}

function makeHeroBullet() {
	var bullet = new Object();
	bullet.x = hero.x + 50;
	bullet.y = hero.y + 12;
	bullet.w = 8;
	bullet.h = 8;
	bullet.alive = true;
	return bullet;
}

function dispatchEnemyPlane() {
	var enemyPlane = makeEnemyPlane();
	enemyPlanes.push(enemyPlane);
}

function makeHeroPlane() {
	var plane = new Object();
	plane.x = 0;
	plane.y = (SCREEN_HEIGHT - PLANE_HEIGHT) / 2;
	plane.w = PLANE_WIDTH;
	plane.h = PLANE_HEIGHT;
	plane.alive = true;
	return plane;
}

function makeEnemyPlane() {
	var maxPossibleY = SCREEN_HEIGHT - PLANE_HEIGHT;
	var plane = new Object();
	plane.x = SCREEN_WIDTH;
	plane.y = randomInt(maxPossibleY);
	plane.w = PLANE_WIDTH;
	plane.h = PLANE_HEIGHT;
	plane.imgIndex = randomInt(2);
	plane.alive = true;
	return plane;
}

function detectCollisions() {
	detectHeroBulletEnemyPlaneCollisions();
	detectHeroEnemyPlaneCollisions();
}

function detectHeroBulletEnemyPlaneCollisions() {
	for(var i = 0; i < heroBullets.length; i++) {
		var bullet = heroBullets[i];
		if(!bullet.alive) {
			continue;
		}
		for(var j = 0; j < enemyPlanes.length; j++) {
			var plane = enemyPlanes[j];
			if(!plane.alive) {
				continue;
			}
			if(rectangles_collide(bullet, plane)) {
				bullet.alive = false;
				plane.alive = false;
				score += 100;
			}
		}
	}
}

function detectHeroEnemyPlaneCollisions() {
	for(var i = 0; i < enemyPlanes.length; i++) {
		var plane = enemyPlanes[i];
		if(!plane.alive) {
			continue;		
		}
		if(rectangles_collide(hero, plane)) {
			hero.alive = false;
			plane.alive = false;
		}
	}
}

function rectangles_collide(rect1, rect2) {
	return rectangles_collide_distance(rect1, rect2, 0);
}

function rectangles_touch(rect1, rect2) {
	return rectangles_collide_distance(rect1, rect2, 1);
}

function rectangles_collide_distance(rect1, rect2, minDistance) {
	// First rectangle
	bottom1 = rect1.y + rect1.h;
	top1 = rect1.y;
	right1 = rect1.x + rect1.w;
	left1 = rect1.x;
	// Second rectangle
	bottom2 = rect2.y + rect2.h;
	top2 = rect2.y;
	right2 = rect2.x + rect2.w;
	left2 = rect2.x;
	// If any of the sides from this are outside of other
	if(bottom1 + minDistance < top2) {
		return false;
	}
	if(top1 - minDistance > bottom2) {
		return false;
	}
	if(right1 + minDistance < left2) {
		return false;
	}
	if(left1 - minDistance > right2) {
		return false;
	}
	// If none of the sides from this are outside other
	return true;
}

function randomInt(maxInt) {
	return Math.floor(Math.random() * maxInt);
}
