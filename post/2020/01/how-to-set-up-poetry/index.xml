<?xml version='1.0' encoding='utf-8'?>
<sb:post
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sb="https://cosmin.hume.ro/project/staticbee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://cosmin.hume.ro/project/staticbee https://cosmin.hume.ro/project/staticbee/staticbee.xsd">
  <sb:title>How to Set up Poetry</sb:title>
  <sb:date>2020-01-12</sb:date>
  <sb:tags>
    <sb:tag>bash</sb:tag>
    <sb:tag>python</sb:tag>
  </sb:tags>
  <sb:categories>
    <sb:category>howto</sb:category>
  </sb:categories>
  <sb:content>
    <p><a href="https://python-poetry.org/">Poetry</a> = Python packaging and dependency management</p>

    <h2>Quick install</h2>
    <sb:code lang="bash">
    <![CDATA[
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
$ source $HOME/.poetry/env
$ poetry completions bash | sudo dd of=/etc/bash_completion.d/poetry.bash-completion
    ]]>
    </sb:code>

    <h2>Explained</h2>
    <sb:code lang="bash">
    <![CDATA[
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
    ]]>
    </sb:code>
    <p>This will add <code>poetry</code> to the <code>PATH</code> variable, in <code>$HOME/.profile</code></p>
    <p>But <a href="https://askubuntu.com/questions/566399/why-isnt-profile-sourced-when-opening-a-terminal">.profile is not sourced when opening a new terminal</a>. We need to re-login to the GUI. For the moment:</p>
    <sb:code lang="bash">
    <![CDATA[
$ source $HOME/.poetry/env
    ]]>
    </sb:code>
    <p>The following should configure bash completion:</p>
    <sb:code lang="bash">
    <![CDATA[
$ poetry completions bash > /etc/bash_completion.d/poetry.bash-completion
    ]]>
    </sb:code>
    <p>It doesn't just work, because the current user does not have write acces to <code>/etc</code>. One possibility would be to run everything in a new shell with <code>sudo</code>:</p>
    <sb:code lang="bash">
    <![CDATA[
$ sudo sh -c 'poetry completions bash > /etc/bash_completion.d/poetry.bash-completion'
    ]]>
    </sb:code>
    <p>This doesn't work either, because the new shell does not have the <code>PATH</code> configured yet. So we need to run <code>poetry</code> in the current shell and do only the redirection with <code>sudo</code>:</p>
    <sb:code lang="bash">
    <![CDATA[
$ poetry completions bash | sudo dd of=/etc/bash_completion.d/poetry.bash-completion
    ]]>
    </sb:code>

    <h2>Sources</h2>
    <dl>
      <dt>Poetry README</dt>
      <dd><a href="https://github.com/python-poetry/poetry">https://github.com/python-poetry/poetry</a></dd>
      <dt>When is .profile sourced</dt>
      <dd><a href="https://askubuntu.com/q/566399/189834">https://askubuntu.com/q/566399/189834</a></dd>
      <dt>Sudo redirect</dt>
      <dd><a href="https://stackoverflow.com/q/82256/479288">https://stackoverflow.com/q/82256/479288</a></dd>
    </dl>
  </sb:content>
</sb:post>
