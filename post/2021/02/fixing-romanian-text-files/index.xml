<?xml version='1.0' encoding='utf-8'?>
<sb:post
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sb="https://cosmin.hume.ro/project/staticbee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://cosmin.hume.ro/project/staticbee https://cosmin.hume.ro/project/staticbee/staticbee.xsd">
  <sb:title>Fixing Romanian Text Files</sb:title>
  <sb:date>2021-02-13</sb:date>
  <sb:tags>
    <sb:tag>bash</sb:tag>
  </sb:tags>
  <sb:categories>
    <sb:category>howto</sb:category>
  </sb:categories>
  <sb:content>
    <h2>Problem</h2>
    <p>You have a text file containing strings in Romanian, with the following defects:
    <ul>
      <li>
        the file is encoded as <a href="https://en.wikipedia.org/wiki/ISO/IEC_8859-2">ISO 8859-2</a>
        (probably produced on a legacy operating system)
      </li>
      <li>
        the diacritics are wrong
        (i.e. with cedilla instead of comma:
        <a href="https://en.wikipedia.org/wiki/S-cedilla">Ş/ş</a>
        instead of <a href="https://en.wikipedia.org/wiki/S-comma">Ș/ș</a>
        and <a href="https://en.wikipedia.org/wiki/%C5%A2">Ţ/ţ</a>
        instead of <a href="https://en.wikipedia.org/wiki/T-comma">Ț/ț</a>)
      </li>
    </ul>
    As a result of the above, a program that assumes <a href="https://en.wikipedia.org/wiki/UTF-8">UTF-8</a>
    for encoding might display ª/º and Þ/þ.
    </p>
    <p>You want to fix the file encoding and to use the correct diacritics.</p>
    <h2>Solution</h2>
    <h3>TL; DR</h3>
    <sb:code lang="bash">
    <![CDATA[
$ iconv -f ISO-8859-2 -t UTF-8 < bad | sed 'y/ŞşŢţÃã/ȘșȚțĂă/' > good
    ]]>
    </sb:code>

    <h3>Step by Step</h3>
    <p>
      Let's take the letter <code>Ș</code> as an example.
      The following table shows the byte representation
      of the majuscule and the minuscule for both wrong and correct versions.
    </p>
    <table>
    <tr><th></th><th>ISO 8859-2</th><th>UTF-8</th></tr>
    <tr><th><code>Ş</code> (cedilla, wrong)</th><td><code>AA</code></td><td><code>C5 9E</code></td></tr>
    <tr><th><code>ş</code> (cedilla, wrong)</th><td><code>BA</code></td><td><code>C5 9F</code></td></tr>
    <tr><th><code>Ș</code> (comma, correct)</th><td>n/a</td>            <td><code>C8 98</code></td></tr>
    <tr><th><code>ș</code> (comma, correct)</th><td>n/a</td>            <td><code>C8 99</code></td></tr>
    </table>
    <p>
      The plan is to start from <code>0xAA</code>,
      convert the entire file to UTF-8 in order to obtain <code>0xC5 0x9E</code>
      and then replace S-cedilla with S-comma, ending up with <code>0xC8 0x98</code>
      (and the same for the other possibly wrong characters).
    </p>
    <p>
      We could also try to replace <code>0xAA</code> with <code>0xC8 0x98</code> directly.
      However, we would have to take care of all non-ASCII characters,
      otherwise we would get a file with mixed (broken) encoding.
      Therefore it's safer to convert the entire file from ISO 8859-2 to UTF-8 as a first step.
    </p>
    <p>
      Let's prepare a file with the wrong encoding and the wrong characters.
      For example, upper S-comma (<code>0xAA</code>) and lower S-comma (<code>0xBA</code>).
      We're going to name the file <code>sh1</code>.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ printf "\xaa\xba" > sh1
    ]]>
    </sb:code>
    <p>Make sure the file is detected as ISO-8859.</p>
    <sb:code lang="bash">
    <![CDATA[
$ file sh1
sh1: ISO-8859 text, with no line terminators
    ]]>
    </sb:code>
    <p>Look at the binary content, make sure the 2 bytes got there as intended.</p>
    <sb:code lang="bash">
    <![CDATA[
$ xxd sh1
00000000: aaba                                     ..
    ]]>
    </sb:code>
    <p>
      Convert the <code>sh1</code> file to UTF-8
      using <a href="https://www.gnu.org/software/libiconv/"><code>iconv</code></a>
      and save the result to a new file, <code>sh2</code>.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ iconv -f "ISO-8859-2" -t "UTF-8" sh1 -o sh2
    ]]>
    </sb:code>
    <p>Make sure the new file is detected as UTF-8.</p>
    <sb:code lang="bash">
    <![CDATA[
$ file sh2
sh2: UTF-8 Unicode text, with no line terminators
    ]]>
    </sb:code>
    <p>
      Look at the binary content of the new file,
      make sure we now have 4 bytes for the UTF-8 representation of the 2 characters.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ xxd sh2
00000000: c59e c59f                                ....
    ]]>
    </sb:code>
    <p>
      Use <a href="https://www.gnu.org/software/sed/"><code>sed</code></a>
      to read from the intermediary file <code>sh2</code>,
      and write to a new file, <code>sh3</code>.
      The <code>y</code> command translates each character to its couterpart.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ sed 'y/ŞşŢţÃã/ȘșȚțĂă/' < sh2 > sh3
    ]]>
    </sb:code>
    <p>
      Look at the binary content of the final file,
      make sure we still have 4 bytes for the UTF-8 representation of the 2 characters,
      but this time with the new values.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ xxd sh3
00000000: c898 c899                                ....
    ]]>
    </sb:code>
    <p>
      Now that we know what works, we can combine everything in one step
      and skip the intermediary file.
    </p>
    <sb:code lang="bash">
    <![CDATA[
$ iconv -f ISO-8859-2 -t UTF-8 < bad | sed 'y/ŞşŢţÃã/ȘșȚțĂă/' > good
    ]]>
    </sb:code>
  </sb:content>
</sb:post>
