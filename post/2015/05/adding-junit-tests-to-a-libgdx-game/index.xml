<?xml version='1.0' encoding='utf-8'?>
<sb:post
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sb="https://cosmin.hume.ro/project/staticbee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://cosmin.hume.ro/project/staticbee https://cosmin.hume.ro/project/staticbee/staticbee.xsd">
  <sb:title>Adding JUnit Tests to a libGDX Game</sb:title>
  <sb:date>2015-05-08</sb:date>
  <sb:tags>
    <sb:tag>java</sb:tag>
    <sb:tag>eclipse</sb:tag>
    <sb:tag>gradle</sb:tag>
    <sb:tag>gamedev</sb:tag>
    <sb:tag>libgdx</sb:tag>
  </sb:tags>
  <sb:categories>
    <sb:category>howto</sb:category>
  </sb:categories>
  <sb:content>
    <p>Consider we have a typical <a href="http://libgdx.badlogicgames.com/">libGDX</a>-based game, such as the one <a href="https://github.com/libgdx/libgdx/wiki/Project-Setup-Gradle">generated</a> with <code>gdx-setup.jar</code>.</p>

    <h2>Tests directory</h2>
    <p>Before we can add <a href="http://junit.org/">JUnit</a> tests to the core module of our game, let's first reorganize the directory structure.</p>
    <p>The standard directory structure is:</p>
    <ul>
      <li><code>src</code> - the Java sources directory</li>
    </ul>
    <p>We'll expand that in order to separate test classes from production classes. The test resource files (if any) should also be separate.</p>
    <ul>
      <li><code>src/main/java</code> - the Java sources directory - the equivalent of the previous <code>src</code> directory</li>
      <li><code>src/test/java</code> - the Java test sources directory - where our unit test classes will reside</li>
      <li><code>src/test/res</code> - the test resources directory - for resource files that are only used by tests</li>
    </ul>
    <p>Open the <code>core/build.gradle</code> and change this line:</p>
    <sb:code lang="groovy">
    <![CDATA[
sourceSets.main.java.srcDirs = [ "src/" ]
    ]]>
    </sb:code>
    <p>to:</p>
    <sb:code lang="groovy">
    <![CDATA[
sourceSets.main.java.srcDirs = [ "src/main/java/" ]
sourceSets.test.java.srcDirs = [ "src/test/java/" ]
sourceSets.test.resources.srcDirs = [ "src/test/res/" ]
    ]]>
    </sb:code>
    <p>Now create the new directories and move the content from the old <code>src</code> to the new location. If you’re using some version control system, don’t forget to move the files in such a way that preserves history (e.g. <code>git mv</code> or <code>svn move</code> instead of <code>mv</code>):</p>
    <sb:code lang="bash">
    <![CDATA[
$ cd core/src
$ mkdir -p main/java test/java test/res
$ ls
com  MyGdxGame.gwt.xml  main  test
$ git mv com *.gwt.xml main/java/
    ]]>
    </sb:code>
    <p>In this example above we had a <code>com</code> package for the sources.</p>
    <p>Now if you refresh the core project in Eclipse, you'll get some compilation errors. That's because the sources have moved from <code>src</code> to <code>src/main/java</code>, but the classpath still only contains <code>src</code>. We'll ask Gradle to update the Eclipse project for us:</p>
    <img alt="screenshot" src="/res/howto/gdx_gradle_refresh.png" />
    <p>Right-click on the core project, and from the <code>Gradle</code> submenu choose <code>Refresh Source Folders</code>.</p>

    <h2>Adding the JUnit dependency</h2>
    <p>Open the <strong>main</strong> <code>build.gradle</code> file and add the following line to the dependencies for the <code>core</code> module:</p>
    <sb:code lang="groovy">
    <![CDATA[
testCompile "junit:junit:4.11"
    ]]>
    </sb:code>
    <p>So you'll end up with something like this:</p>
    <sb:code lang="groovy">
    <![CDATA[
project(":core") {
    apply plugin: "java"
 
    dependencies {
        compile "com.badlogicgames.gdx:gdx:$gdxVersion"
        testCompile "junit:junit:4.11"
    }
}
    ]]>
    </sb:code>
    <p>Now to let Eclipse know that Gradle got new dependencies, right-click on the <code>core</code> project, and from the <code>Gradle</code> submenu choose <code>Refresh Dependencies</code>.</p>
    <p>That's it, now JUnit should be available in the <code>core</code>.</p>
  </sb:content>
</sb:post>
