<?xml version='1.0' encoding='utf-8'?>
<sb:post
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sb="https://cosmin.hume.ro/project/staticbee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://cosmin.hume.ro/project/staticbee https://cosmin.hume.ro/project/staticbee/staticbee.xsd">
  <sb:title>soju on YunoHost</sb:title>
  <sb:date>2022-12-27</sb:date>
  <sb:tags>
    <sb:tag>self-hosting</sb:tag>
  </sb:tags>
  <sb:categories>
    <sb:category>howto</sb:category>
  </sb:categories>
  <sb:content>
    <p>
      Steps to install the
      <a href="https://git.sr.ht/~emersion/soju">soju</a>
      IRC bouncer on
      <a href="https://yunohost.org/">YunoHost</a> 11.0:
    </p>
    <ol>
      <li>
	Open TCP port <code>6697</code>
	from YunoHost (in "Tools" / "Firewall").
      </li>
      <li>
	Install <code>scdoc</code>:
    <sb:code lang="bash">
      <![CDATA[
sudo apt install scdoc
    ]]>
    </sb:code>
      </li>
      <li>
	Clone the <a href="https://git.sr.ht/~emersion/soju">repo</a>
	then build and install the project with <code>make</code>
	as instructed in the <code>README</code>.
	<ul>
	  <li>
	    If the build fails because the <code>go</code> version
	    is not new enough, then get the newest Go from
	    <a href="https://go.dev/dl/">go.dev</a> and unpack it to
	    <code>~/.local/go</code> then adjust
	    <code>~/.profile</code>
	    to have it in your <code>$PATH</code>:
	    <sb:code lang="bash">
	      <![CDATA[
if [ -d "$HOME/.local/go/bin" ] ; then
    PATH="$HOME/.local/go/bin:$PATH"
fi
	      ]]>
	    </sb:code>
	  </li>
	</ul>
      </li>
      <li>
	<p>
          We'll need SSL certificates.
	  We can use the ones we already have from YunoHost's
	  Let's Encrypt configuration.
	</p>
	<p>
	  Look in <code>/etc/yunohost/certs</code>
	  and make sure you have files like
	  <code>/etc/yunohost/certs/example.com/crt.pem</code>
	</p>
	<p>
	  Also, observe that the certificate files are only accessible
	  to users in the <code>ssl-cert</code> group.
	</p>
      </li>
      <li>
	Create a system user that will run <code>soju</code>
	and add it to the <code>ssl-cert</code> group.
	For now allow it to have a login shell so we can set it up;
	we'll remove the shell at the end.
	<sb:code lang="bash">
	  <![CDATA[
sudo mkdir /var/lib/soju

sudo useradd \
  --system \
  --comment "Account for soju to run as" \
  --shell /bin/bash \
  --home-dir /var/lib/soju soju

sudo chown "soju:soju" /var/lib/soju

sudo usermod -a -G ssl-cert soju
	  ]]>
	</sb:code>
      </li>
      <li>
	Login as <code>soju</code>:
	<sb:code lang="bash">
	  <![CDATA[
sudo su - soju
	  ]]>
	</sb:code>
      </li>
      <li>
	<p>
	  Create a user for the bouncer:
	</p>
	<sb:code lang="bash">
	  <![CDATA[
sojudb create-user cosmin -admin
	  ]]>
	</sb:code>
	<p>
          This will create <code>main.db</code>
	  in the current directory.
	</p>
      </li>
      <li>
	<p>
	  Create a config file, let's say
	  <code>~/.config/soju/soju.config</code>,
	  with the following content:
	</p>
	<sb:code lang="bash">
	  <![CDATA[
tls /etc/yunohost/certs/example.com/crt.pem /etc/yunohost/certs/example.com/key.pem
message-store fs /var/lib/soju/messages
db sqlite3 /var/lib/soju/main.db
	  ]]>
	</sb:code>
      </li>
      <li>
	<p>Start soju, just to check that it works:</p>
	<sb:code lang="bash">
	  <![CDATA[
soju -config ~/.config/soju/soju.config
	  ]]>
	</sb:code>
      </li>
      <li>
	Try to connect with your favorite IRC client
	directly to your YunoHost machine on port <code>6697</code>
	and using the account defined earlier.
      </li>
      <li>
	Stop <code>soju</code>
	and logout from <code>soju</code>'s account.
      </li>
      <li>
	<p>
	  Disable the login shell for the <code>soju</code> user:
	</p>
	<sb:code lang="bash">
	  <![CDATA[
sudo usermod --shell /usr/sbin/nologin soju
	  ]]>
	</sb:code>
      </li>
      <li>
	<p>
	  Create a configuration file for a systemd service,
	  in <code>/etc/systemd/system/soju.service</code>:
	</p>
	<sb:code lang="ini">
	  <![CDATA[
[Unit]
Description=soju IRC bouncer
After=network.target

[Service]
WorkingDirectory=/var/lib/soju
ExecStart=/usr/local/bin/soju -config /var/lib/soju/.config/soju/soju.config
User=soju

[Install]
WantedBy=multi-user.target
	  ]]>
	</sb:code>
      </li>
      <li>
	<p>
	  Enable and start the new service:
	</p>
	<sb:code lang="bash">
	  <![CDATA[
sudo systemctl enable soju
sudo systemctl start soju
	  ]]>
	</sb:code>
	<p>
	  Verify that you can still connect from the IRC client.
	</p>
      </li>
    </ol>
  </sb:content>
</sb:post>
