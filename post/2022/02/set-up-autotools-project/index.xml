<?xml version='1.0' encoding='utf-8'?>
<sb:post
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:sb="https://cosmin.hume.ro/project/staticbee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://cosmin.hume.ro/project/staticbee https://cosmin.hume.ro/project/staticbee/staticbee.xsd">
  <sb:title>
    How to Set Up a Project With Autotools
  </sb:title>
  <sb:date>2022-02-01</sb:date>
  <sb:tags>
    <sb:tag>autotools</sb:tag>
    <sb:tag>c</sb:tag>
  </sb:tags>
  <sb:categories>
    <sb:category>tutorial</sb:category>
  </sb:categories>
  <sb:content>
    <p>
      Let's see how to set up
      <a href="https://en.wikipedia.org/wiki/GNU_Autotools">
	GNU Autotools
      </a>
      for building a new project!
    </p>
    <p>
      Today we're starting a new project
      written in the C language.
    </p>
    <p>
      The project will have a dependency
      to make it more interesting.
      We really can use any library, and for this example we'll pick
      the <a href="https://libsdl.org/">SDL</a> library.
    </p>
    <p>
      Last but not least, our project needs a name.
      Since libSDL is usually powering games, we could imagine
      our project is a game. Why not call it "Aventura"?
    </p>
    <h2>Prerequisites</h2>
    <p>
      Before starting, if you're not familiar with Autotools,
      take some time and go through the
      <a href="https://www.lrde.epita.fr/~adl/autotools.html">
	Autotools tutorial</a>,
      an excellent slideshow by
      <a href="https://www.lrde.epita.fr/~adl/">
        Alexandre Duret-Lutz</a>.
    </p>
    <p>
      Then we need to install all the packages we need.
    </p>
    <p>
      Let's make sure we have Autotools.
      For Debian, that can be done with:
    </p>
    <sb:code lang="bash">
    <![CDATA[
sudo apt install autoconf automake libtool gettext
    ]]>
    </sb:code>
    <p>
      Also, since we picked SDL as a project dependency, let's
      install the SDL development package:
    </p>
    <sb:code lang="bash">
    <![CDATA[
sudo apt install libsdl2-dev
    ]]>
    </sb:code>
    <h2>The implementation</h2>
    <ol>
      <li>
	Prepare the directory structure.
	We need a <code>src</code> directory for the source code,
	where we begin with a single file, <code>main.c</code>.
    <div class="highlight">
      <pre>
	<![CDATA[
.
└── src
    └── main.c
	]]>
      </pre>
    </div>
    </li>
    <li>
      Edit <code>src/main.c</code>
      and add the following code, which attempts to initialize
      the video subsystem of SDL and then cleans up and exits.
      It's like "Hello, World!" for SDL.
    <sb:code lang="c">
    <![CDATA[
#include "SDL.h"
#include <stdio.h>

int
main (void)
{
  if (SDL_Init (SDL_INIT_VIDEO) < 0)
    {
      fprintf (stderr, "SDL could not be initialized. SDL_Error: %s\n",
	       SDL_GetError ());
      return 1;
    }
  atexit (SDL_Quit);
  return 0;
}
    ]]>
    </sb:code>
    <p>
      How would we compile this without Autotools?
    </p>
    <p>
      A first attempt might be:
    </p>
    <sb:code lang="bash">
    <![CDATA[
gcc -o src/aventura src/main.c
    ]]>
    </sb:code>
    <p>
      However that would not be enough, because <code>SDL.h</code>
      is most likely not located directly
      in a standard include directory (such as
      <code>/usr/include</code>). It's probably under
      <code>/usr/include/SDL2</code>.
    </p>
    <p>
      You could specify the include path with <code>-I</code>:
    </p>
    <sb:code lang="bash">
    <![CDATA[
gcc -I/usr/include/SDL2 -o src/aventura src/main.c
    ]]>
    </sb:code>
    <p>
      It would compile, but after that you'd get linker errors.
      You would need to explicitly link against SDL2 with
      <code>-lSDL2</code>:
    </p>
    <sb:code lang="bash">
    <![CDATA[
gcc -I/usr/include/SDL2 -o src/aventura src/main.c -lSDL2
    ]]>
    </sb:code>
    <p>
      An easier and more portable way of getting the build flags
      in place would be to use
      <a href="https://en.wikipedia.org/wiki/Pkg-config">pkg-config</a>.
    </p>
    <p>
      First we would call <code>pkg-config --list-all</code> and look
      for "sdl" in its output:
    </p>
    <sb:code lang="bash">
    <![CDATA[
pkg-config --list-all | grep -i sdl
    ]]>
    </sb:code>
    <p>
      The above would tell us that the name of the package for SDL
      is <code>sdl2</code>, and we could use that name to get the
      build flags:
    </p>
    <sb:code lang="bash">
    <![CDATA[
pkg-config --cflags --libs sdl2
    ]]>
    </sb:code>
    <p>
      Then we could use the output of <code>pkg-config</code>
      in the call to <code>gcc</code>:
    </p>
    <sb:code lang="bash">
    <![CDATA[
gcc -o main $(pkg-config --cflags --libs sdl2) src/main.c
    ]]>
    </sb:code>
    <p>
      That's better, but at this point it's already too complicated
      to compile directly (and we just started).
      Better leave this to the build system.
    </p>
  </li>
  <li>
    <p>
      Let's add one Makefile template
      (named <code>Makefile.am</code>)
      in the root directory and another one in <code>src</code>.
    </p>
    <sb:code lang="bash">
      <![CDATA[
touch {.,src}/Makefile.am
      ]]>
    </sb:code>
    <p>
      Now the project tree should look like this:
    </p>
    <div class="highlight">
      <pre>
	<![CDATA[
.
├── Makefile.am
└── src
    ├── main.c
    └── Makefile.am
	]]>
      </pre>
    </div>
    </li>
    <li>
      Edit the top level <code>Makefile.am</code>
      and add the following line,
      which tells Automake which subdirectories to build
      (in our case, <code>src</code>).
    <div class="highlight">
      <pre>
	<![CDATA[
SUBDIRS = src
	]]>
      </pre>
    </div>
    </li>
    <li>
      Edit <code>src/Makefile.am</code>
      and add the following two lines,
      which tell Automake the names of the binaries we want to build
      (in our case, <code>aventura</code>)
      and the lists of source files to compile
      for each of these binaries (<code>main.c</code> for now).
    <div class="highlight">
      <pre>
	<![CDATA[
bin_PROGRAMS = aventura
aventura_SOURCES = main.c
	]]>
      </pre>
    </div>
    </li>
    <li>Run <code>autoscan</code>.
    <p>
      This will produce <code>configure.scan</code>,
      which is can be used as a starting point for
      <code>configure.ac</code>:
    </p>
    <div class="highlight">
      <pre>
	<![CDATA[
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.71])
AC_INIT([FULL-PACKAGE-NAME], [VERSION], [BUG-REPORT-ADDRESS])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CC

# Checks for libraries.

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.
AC_CHECK_FUNCS([atexit])

AC_CONFIG_FILES([Makefile
                 src/Makefile])
AC_OUTPUT
	]]>
      </pre>
    </div>
    </li>
    <li>Rename <code>configure.scan</code>
    to <code>configure.ac</code>.</li>
    <li>Edit <code>configure.ac</code>:
    <ul>
      <li>Fill the values for the project name, the version
      and the bug report address</li>
      <li>Call <code>AM_INIT_AUTOMAKE</code></li>
      <li>
	Call <code>PKG_CHECK_MODULES</code> to define the dependency.
	This macro is an interface between autoconf and
	<code>pkg-config</code>.
	We already know the name of the package is <code>sdl2</code>.
      </li>
    </ul>
    <sb:code lang="diff">
      <![CDATA[
5c5,6
< AC_INIT([FULL-PACKAGE-NAME], [VERSION], [BUG-REPORT-ADDRESS])
---
> AC_INIT([aventura], [0.1], [someone@example.com])
> AM_INIT_AUTOMAKE([foreign -Wall -Werror])
12a14,20
> PKG_CHECK_MODULES(
>   [SDL2],
>   [sdl2 > 2.0.3],
>   [CFLAGS="$CFLAGS $SDL2_CFLAGS"
>    LIBS="$SDL2_LIBS $LIBS"],
>   AC_MSG_WARN($SDL2_PKG_ERRORS)
> )
      ]]>
    </sb:code>
    </li>
    <li>Run <code>autoreconf --install</code></li>
    </ol>
    <p>Now everything is ready.
    You can run <code>./configure</code>
    followed by <code>make</code>.</p>
    <p>The executable is <code>./src/aventura</code>.</p>
  </sb:content>
</sb:post>
